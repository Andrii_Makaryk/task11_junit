package com.makaryk.tasks;

public class SimpleClass implements SimpleExample {
    @Override
    public int add(int a, int b) {
        return a+b;
    }

    @Override
    public int minus(int a, int b) {
        return a-b;
    }

    @Override
    public int mult(int a, int b) {
        return a*b;
    }

    @Override
    public int div(int a, int b) {
        return a/b;
    }

    @Override
    public boolean checkToStrings(String str1, String str2) {
        boolean result=false;
        if(str1.equals(str2))result=true;
        else if(!str1.equals(str2))result=false;
        return result;
    }

    @Override
    public void printMessage() {
        System.out.println("Vivere est militare.");
    }

    @Override
    public void someMethod() {
        System.out.println("Bingo!!!");
    }


}
