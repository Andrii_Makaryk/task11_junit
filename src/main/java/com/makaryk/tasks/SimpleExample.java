package com.makaryk.tasks;

import javafx.beans.value.ObservableBooleanValue;

public interface SimpleExample {
    public int add(int a, int b);

    public int minus(int a, int b);

    public int mult(int a, int b);

    public int div(int a, int b);

    public boolean checkToStrings(String str1, String str2);

    public void printMessage();

    public void someMethod();

}
