package com.makaryk.tasks.tests;

import com.makaryk.tasks.SimpleClass;
import org.junit.Assert;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

public class TestSimpleClass {

    SimpleClass simpleClass=new SimpleClass();
    int result;

    @Test
    public void testInitialisation(){
        Assert.assertNotNull(simpleClass);
    }

    @Test
    public void testAdd() {
         result = simpleClass.add(2, 5);
        Assert.assertTrue(result==7);
    }
    @Test
    public void testMinus() {
    result = simpleClass.minus(8, 1);
    Assert.assertEquals(7, result);
    }
    @Test
    public void testMult(){
        result=simpleClass.mult(2,4);
        Assert.assertFalse(result==6);
    }
    @Test
    public void testDiv(){
        result=simpleClass.div(15,3);
        Assert.assertNotEquals(4,result);
    }
    @Test
    public void testSomeMethod(){
        ByteArrayOutputStream out=new ByteArrayOutputStream();
        System.setOut(new PrintStream(out));
        simpleClass.someMethod();
        Assert.assertEquals(out.toString(),"Bingo!!!");
    }
    @Test
    public void testPrintMessage(){
        ByteArrayOutputStream outTwo=new ByteArrayOutputStream();
        System.setOut(new PrintStream(outTwo));
        simpleClass.printMessage();
        Assert.assertEquals(outTwo.toString(),"Vivere est militare.");
    }
    @Test
    public void testCheckToStrings(){
        Assert.assertTrue(simpleClass.checkToStrings("ABAB","ABAB"));
    }
}
