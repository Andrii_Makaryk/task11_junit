package com.makaryk.tasks;

import com.makaryk.tasks.tasks.Calculator;
import com.makaryk.tasks.tasks.SimpleCalculator;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.stubbing.OngoingStubbing;

import static javafx.beans.binding.Bindings.when;
import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
public class TestCalculatorMockito {
    @InjectMocks
    Calculator example;

    @Mock
    SimpleCalculator labal;

    @Test
    public void testAdd() {
        SimpleCalculator simple = Mockito.mock(SimpleCalculator.class);
        Calculator calc = new Calculator(simple);

        when(simple.add(12.0, 18.0)).then(3.0);
        Assert.assertEquals(calc.adding(12, 18), 30);
    }

    private <T> OngoingStubbing<T> when(double add) {
    }


}
